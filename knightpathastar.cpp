#include "knightpathastar.h"

#include <QMap>

extern uint qHash(const QPoint& key);

KnightPathAStar::KnightPathAStar()
{
}

const std::string& KnightPathAStar::Algorithm() const
{
    static const std::string& alg = "A* search";
    return alg;
}

std::deque<QPoint> KnightPathAStar::GetPath(const QPoint& start, const QPoint& stop,
                            const int width, const int height)
{
    QMap<uint,int> Fv;
    QMap<uint,QPoint> Parent;
    std::deque<QPoint> moves = { start };
    Fv[qHash(moves[0])] = GetDist(start, stop);
    Parent[qHash(moves[0])] = QPoint(-1, -1);
    QPoint move;
    while (!moves.empty())
    {
        std::sort(moves.begin(), moves.end(), [&Fv,&start,&stop, this](const QPoint& l, const QPoint& r)
        {
            const uint& hL = qHash(l);
            const uint& hR = qHash(r);
            if (!Fv.contains(hL))
            {
                Fv[hL] = GetDist(start, l) + GetDist(l, stop);
            }
            if (!Fv.contains(hR))
            {
                Fv[hR] = GetDist(start, r) + GetDist(r, stop);
            }
            return Fv[hL] < Fv[hR];
        });
        move = moves[0];
        if (move == stop)
        {
            break;
        }
        const uint& hMove = qHash(move);
        moves.erase(std::find(moves.begin(), moves.end(), move));
        auto succ = GetLegalMoves(move, width, height);
        for (uint i = 0; i != succ.size();)
        {
            const uint hSucc = qHash(succ[i]);
            const int& f = Fv[hMove] + GetDist(move, succ[i]) + GetDist(succ[i], stop);
            if (!Fv.contains(hSucc) || Fv[hSucc] > f)
            {
                Fv[hSucc] = f;
                Parent[hSucc] = move;
                ++i;
            }
            else
            {
                succ.erase(succ.begin() + i);
            }
        }
        moves.insert(moves.end(), succ.begin(), succ.end());
    }
    std::deque<QPoint> path;
    while (move.x() >= 0 && move.y() >= 0)
    {
        path.push_front(move);
        move = Parent[qHash(move)];
    }
    path.erase(path.begin());
    return path;
}
