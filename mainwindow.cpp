#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QMessageBox>
#include <QPoint>
#include <QRandomGenerator>
#include <QString>

#include <board.h>
#include <knightpathastar.h>
#include <knightpathdepthfirst.h>

#include <deque>
#include <iostream>
#include <sstream>

static const QString& AppVersion = "1.0";

std::ostream& operator<<(std::ostream& os, const QPoint& p)
{
    os << "(" << p.x() << "," << p.y() << ")";
    return os;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowIcon(QIcon("knight.ico"));
    on_spnBoardWidth_valueChanged(ui->spnBoardWidth->value());
    GameBoard.show();
    resize(0, 0);
    setMinimumSize(size());
    setMaximumSize(size());
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent*)
{
    GameBoard.close();
}

QString MainWindow::PrintElapsedTime(std::ostream& out, const qint64& ms) const
{
    const qint64& sec = ms / 1000 % 60;
    const qint64& min = ms / 1000 / 60;
    std::stringstream ss;
    ss << "Elapsed " << min << "min " << sec << "s " << ms % 1000 << "ms";
    std::string str = ss.str();
    out << str << std::endl;
    return QString::fromStdString(str);
}

void MainWindow::on_spnBoardWidth_valueChanged(int arg1)
{
    if (ui->chkBoardLock->isChecked())
    {
        ui->spnBoardHeight->setValue(arg1);
    }
    GameBoard.SetDimensions(QSize(ui->spnBoardWidth->value(),
                            ui->spnBoardHeight->value()));
    ui->spnStartY->setMaximum(arg1 - 1);
    ui->spnGoalY->setMaximum(arg1 - 1);
}

void MainWindow::on_spnBoardHeight_valueChanged(int arg1)
{
    if (ui->chkBoardLock->isChecked())
    {
        ui->spnBoardWidth->setValue(arg1);
    }
    GameBoard.SetDimensions(QSize(ui->spnBoardWidth->value(),
                            ui->spnBoardHeight->value()));
    ui->spnStartX->setMaximum(arg1 - 1);
    ui->spnGoalX->setMaximum(arg1 -1);
}

void MainWindow::on_pshAbout_clicked()
{
    QMessageBox::about(this, "About", "Knight's Shortest Path ver. " + AppVersion +
                                                                       "\n\nThis piece of software is distributed under terms of GNU GPLv3 license."
                                                                       "\n\nCopyright (c) 2023 by LK.");
}

void MainWindow::on_pshAboutQt_clicked()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::on_pshSolve_clicked()
{    
    const QSize board(ui->spnBoardWidth->value(),
                      ui->spnBoardHeight->value());

    const QPoint from(ui->spnStartX->value(), ui->spnStartY->value());
    const QPoint to(ui->spnGoalX->value(), ui->spnGoalY->value());

    KnightPathAStar knightPathAStar;
    KnightPathDepthFirst knightPathDepthFirst;

    QList<KnightPathBase*> knightPath;
    if (ui->chkAlgAStar->isChecked())
    {
        knightPath.append(&knightPathAStar);
    }
    if (ui->chkAlgDFS->isChecked())
    {
        knightPath.append(&knightPathDepthFirst);
    }

    statusBar()->showMessage("");
    GameBoard.ClearMoves();

    qint64 elapsed = 0;
    std::stringstream output;
    std::deque<QPoint> path;

    output << board.width() << "x" << board.height() << " board: start="
           << from << " goal=" << to << std::endl;

    for (auto& alg : knightPath)
    {
        path = alg->GetKnightPath(from, to, board.width(), board.height());

        output << alg->Algorithm() << std::endl;
        for (const auto& p : path)
        {
            output << p << " ";
        }
        output << std::endl;

        const qint64& el = alg->ElapsedTime();
        PrintElapsedTime(output, el);
        elapsed += el;

        path.push_front(from);

        GameBoard.AddMoves(path);
    }

    const std::string& txt = output.str();
    std::cout << txt << std::endl;
    ui->txtOutput->setPlainText(QString::fromStdString(txt));

    statusBar()->showMessage(PrintElapsedTime(output, elapsed));

    std::stringstream ss;
    ss << "Knight's shortest path from " << from << " to " << to << " has " << path.size() - 1 << " move(s)." << std::endl;

    GameBoard.setWindowTitle(QString::fromStdString(ss.str()));
    GameBoard.show();
    GameBoard.SetDimensions(board);
}

void MainWindow::on_pshDiag1_clicked()
{
    ui->spnStartX->setValue(0);
    ui->spnStartY->setValue(0);
    ui->spnGoalX->setValue(ui->spnBoardHeight->value() - 1);
    ui->spnGoalY->setValue(ui->spnBoardWidth->value() - 1);
}

void MainWindow::on_pshDiag2_clicked()
{
    ui->spnStartX->setValue(ui->spnBoardHeight->value() - 1);
    ui->spnStartY->setValue(0);
    ui->spnGoalX->setValue(0);
    ui->spnGoalY->setValue(ui->spnBoardWidth->value() - 1);
}

uint MainWindow::GetRandomCoordinate(const int max) const
{
    return QRandomGenerator::global()->bounded(0, max);
}

void MainWindow::on_pshRandom_clicked()
{
    ui->spnStartX->setValue(GetRandomCoordinate(ui->spnBoardHeight->value()));
    ui->spnStartY->setValue(GetRandomCoordinate(ui->spnBoardWidth->value()));
    ui->spnGoalX->setValue(GetRandomCoordinate(ui->spnBoardHeight->value()));
    ui->spnGoalY->setValue(GetRandomCoordinate(ui->spnBoardWidth->value()));
}
