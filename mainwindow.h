#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <board.h>

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    protected:
        void closeEvent(QCloseEvent* ev) override;

    private:
        QString PrintElapsedTime(std::ostream &out, const qint64 &ms) const;
        uint GetRandomCoordinate(const int max) const;

    private slots:
        void on_spnBoardWidth_valueChanged(int arg1);
        void on_spnBoardHeight_valueChanged(int arg1);
        void on_pshAbout_clicked();
        void on_pshAboutQt_clicked();
        void on_pshSolve_clicked();
        void on_pshDiag1_clicked();
        void on_pshDiag2_clicked();
        void on_pshRandom_clicked();

    private:
        Ui::MainWindow *ui;
        Board GameBoard;
};

#endif // MAINWINDOW_H
