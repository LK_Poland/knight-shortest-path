#ifndef KNIGHTPATHDEPTHFIRST_H
#define KNIGHTPATHDEPTHFIRST_H

#include <QSet>

#include "knightpathbase.h"

class KnightPathDepthFirst : public KnightPathBase
{
    public:
        KnightPathDepthFirst();

        const std::string& Algorithm() const override;

    protected:
        std::deque<QPoint> GetPath(const QPoint& from, const QPoint& to,
                                   const int width, const int height) override;

    private:
        void GetPath(std::deque<QPoint>& path, std::deque<QPoint>& bestPath, QSet<QPoint> visited,
                        const int depth, int &minDepth,
                        const QPoint& start, const QPoint& from, const QPoint& to,
                        const int width, const int height);
};

#endif // KNIGHTPATHDEPTHFIRST_H
