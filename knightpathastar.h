#ifndef KNIGHTPATHASTAR_H
#define KNIGHTPATHASTAR_H

#include "knightpathbase.h"

class KnightPathAStar : public KnightPathBase
{
    public:
        KnightPathAStar();

        const std::string &Algorithm() const override;

    protected:
        std::deque<QPoint> GetPath(const QPoint &start, const QPoint &stop,
                                   const int width, const int height) override;
};

#endif // KNIGHTPATHASTAR_H
