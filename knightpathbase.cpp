#include "knightpathbase.h"

uint qHash(const QPoint& key)
{
    return qHash(QPair<int,int>(key.x(), key.y()));
}

KnightPathBase::KnightPathBase()
{
}

std::deque<QPoint> KnightPathBase::GetKnightPath(const QPoint &start, const QPoint &stop,
                                                 const int width, const int height)
{
    timer.restart();
    return GetPath(start, stop, width, height);
}

qint64 KnightPathBase::ElapsedTime()
{
    return timer.elapsed();
}

std::deque<QPoint> KnightPathBase::GetLegalMoves(const QPoint& from,
                                 const int width, const int height)
{
    std::deque<QPoint> pat = {{-2, -1}, {-2, 1}, {-1, 2}, { 1, 2},
                              { 2,  1}, { 2,-1}, { 1,-2}, {-1,-2}};
    std::deque<QPoint> out;
    for (const QPoint& p : pat)
    {
        const QPoint& np(from + p);
        if (np.x() >= 0 && np.x() < height &&
            np.y() >= 0 && np.y() < width)
        {
            out.push_back(np);
        }
    }
    return out;
}

int KnightPathBase::GetDist(const QPoint& p1, const QPoint& p2)
{
    return std::abs(p1.x() - p2.x()) + std::abs(p1.y() - p2.y());
}
