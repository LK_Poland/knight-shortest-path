#include "board.h"
#include "ui_board.h"

#include <QPainter>

Board::Board() :
    QWidget(nullptr),
    ui(new Ui::Board)
{
    ui->setupUi(this);
}

Board::~Board()
{
    delete ui;
}

void Board::SetDimensions(const QSize& dim)
{
    this->dim = dim;
    repaint();
}

void Board::AddMoves(std::deque<QPoint>& mvs)
{
    moves.append(mvs);
}

void Board::ClearMoves()
{
    moves.clear();
}

void Board::paintEvent(QPaintEvent*)
{
    QPainter p(this);
    const int& rectWidth = width() / dim.width();
    const int& rectHeight = height() / dim.height();
    const int& dX = width() % dim.width() / 2;
    const int& dY = height() % dim.height() / 2;
    for (int i = 0 ; i < dim.width() ; ++i)
    {
        int c = i;
        for (int j = 0 ; j < dim.height() ; ++j)
        {
            p.setPen(Qt::black);
            p.setBrush(QBrush(c++ % 2 ? Qt::gray : Qt::darkGray));
            p.setFont(QFont("Arial", 10));
            p.drawRect(dX + i * rectWidth, dY + j * rectHeight, rectWidth, rectHeight);
            p.drawText(dX + i * rectWidth + 5, dY + j * rectHeight + 20, QString("(%1,%2)").arg(j).arg(i));
            p.setFont(QFont("Arial", 16, QFont::Bold));
            static const Qt::GlobalColor colors[] = {Qt::yellow, Qt::red, Qt::blue, Qt::white};
            const int colorsCount = sizeof(colors) / sizeof(int);
            for (int k = 0; k != std::min(moves.size(), colorsCount) ; ++k)
            {
                p.setPen(QPen(colors[k]));
                for (uint l = 0 ; l < moves[k].size() ; ++l)
                {
                    const QPoint& move = moves[k][l];
                    if (move.x() == j && move.y() == i)
                    {
                        p.drawText(dX + i * rectWidth + 5, dY + j * rectHeight + rectHeight - 5,
                                   QString::number(l));
                        break;
                    }
                }
            }
        }
    }
}
