#ifndef BOARD_H
#define BOARD_H

#include <QList>
#include <QRect>
#include <QWidget>

#include <deque>

namespace Ui
{
    class Board;
}

class Board : public QWidget
{
    Q_OBJECT

    public:
        explicit Board();
        ~Board();

        void SetDimensions(const QSize& dim);
        void AddMoves(std::deque<QPoint>&);
        void ClearMoves();

    protected:
        void paintEvent(QPaintEvent*) override;

    private:
        Ui::Board *ui;
        QSize dim;
        QList<std::deque<QPoint>> moves;
};

#endif // BOARD_H
