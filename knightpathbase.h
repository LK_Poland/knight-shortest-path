#ifndef KNIGHTPATHBASE_H
#define KNIGHTPATHBASE_H

#include <QElapsedTimer>
#include <QHash>
#include <QPair>
#include <QPoint>

#include <deque>
#include <string>

class KnightPathBase
{
    public:
        KnightPathBase();

        virtual std::deque<QPoint> GetKnightPath(const QPoint& start, const QPoint& stop,
                                                    const int width, const int height);
        qint64 ElapsedTime();
        virtual const std::string& Algorithm() const = 0;

    protected:
        virtual std::deque<QPoint> GetPath(const QPoint& start, const QPoint& stop,
                                            const int width, const int height) = 0;
        std::deque<QPoint> GetLegalMoves(const QPoint &from, const int width, const int height);
        int GetDist(const QPoint &p1, const QPoint &p2);

    private:
        QElapsedTimer timer;
};

#endif // KNIGHTPATHBASE_H
