#include "knightpathdepthfirst.h"

extern uint qHash(const QPoint& key);

KnightPathDepthFirst::KnightPathDepthFirst()
{
}

const std::string& KnightPathDepthFirst::Algorithm() const
{
    static const std::string& alg = "Depth-first search";
    return alg;
}

void KnightPathDepthFirst::GetPath(std::deque<QPoint>& path, std::deque<QPoint>& bestPath,
                                    QSet<QPoint> visited,
                                    const int depth, int& minDepth, const QPoint& start,
                                    const QPoint& from, const QPoint& to,
                                    const int width, const int height)
{
    if (depth >= minDepth)
    {
        return;
    }
    auto moves = GetLegalMoves(from, width, height);
    std::sort(moves.begin(), moves.end(), [&to, this](const QPoint& l, const QPoint& r)
    {
        const int lDist = GetDist(l, to);
        const int rDist = GetDist(r, to);
        return lDist < rDist;
    });
    for (const auto& move : moves)
    {
        if (visited.contains(move))
        {
            continue;
        }
        else if (move == to)
        {
            bestPath = path;
            bestPath.push_back(move);
            minDepth = depth;
            return;
        }
        visited.insert(move);
        path.push_back(move);
        GetPath(path, bestPath, visited, depth + 1, minDepth, start, move, to, width, height);
        path.pop_back();
    }
}

std::deque<QPoint> KnightPathDepthFirst::GetPath(const QPoint& from, const QPoint& to,
                                                    const int width, const int height)
{
    std::deque<QPoint> path, bestPath;
    QSet<QPoint> visited { from };
    int minDepth = INT_MAX;
    if (from != to)
    {
        GetPath(path, bestPath, visited, 0, minDepth, from, from, to, width, height);
    }
    return bestPath;
}
